CREATE DATABASE stock_predictions

CREATE TABLE stock_list (
  id INT IDENTITY(1,1),
  short_code VARCHAR(255),
  last_modified DATETIME NULL,
  sector VARCHAR(255) NULL,
  three_month_close DECIMAL(10, 2) NULL,
  six_month_close DECIMAL(10, 2) NULL,
  nine_month_close DECIMAL(10, 2) NULL,
  current_price DECIMAL(10, 2) NULL
  market_cap BIGINT NULL,
);

CREATE TABLE stock_trade_history (
  trade_id INT IDENTITY(1,1),
  stock_id INT,
  security VARCHAR(255),
  trade_purchase_price DECIMAL(10, 2),
  trade_selling_price DECIMAL(10, 2) NULL,
  trade_purchase_date DATE,
  trade_selling_date DATE NULL,
  trade_sector VARCHAR(255),
  trade_json VARCHAR(255) NULL,
  trade_return DECIMAL(10,2) NULL,
  trade_days_length INT NULL
);

CREATE TABLE snap_shot_data (
  id INT IDENTITY(1,1),
  security VARCHAR(255),
  label VARCHAR(255),
  data VARCHAR(255),
  points INT NULL
);

CREATE TABLE recommendation (
  id INT IDENTITY(1,1),
  market_cap VARCHAR(255),
  time_frame VARCHAR(255),
  label VARCHAR(255),
  min DECIMAL(20,2),
  max DECIMAL(20,2),
  sector VARCHAR(255)
);

CREATE TABLE app_settings_stock_types (
    id INT IDENTITY(1,1),
    name VARCHAR(500)
);

CREATE TABLE app_settings_stock_types_attributes (
    id INT IDENTITY(1,1),
    stock_type_id INT,
    attribute VARCHAR(255),
    data VARCHAR(255)
);
