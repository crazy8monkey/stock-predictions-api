﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using stock_predictions_api.Classes;
using stock_predictions_api.Configeration;
using stock_predictions_api.Models.DB;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace stock_predictions_api.Controllers
{
    [Produces("application/json")]
    [Route("api/snap-shot")]
    public class SnapShotController : Controller
    {

        private readonly SnapShotData _snapShotData;
        private readonly StockPredictionDBContext _stockDBConext;

        public SnapShotController(IConfigurationManager iconfiguration)
        {
            _stockDBConext = new StockPredictionDBContext(iconfiguration);
            _snapShotData = new SnapShotData(_stockDBConext);
        }

        [HttpGet("{security}")]
        public JsonResult List(string security)
        {
            return Json(_snapShotData.List(security));
        }

        [HttpPut("{security}")]
        public JsonResult UpdateData(string security, [FromBody] List<SnapShotDataModel> updateData)
        {
            Console.WriteLine("Update manual test => {0}", updateData.Count());
            //return Json(new { success = true });
            return Json(_snapShotData.UpdateManual(security, updateData));
        }
    }
}
