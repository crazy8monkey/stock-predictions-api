﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using stock_predictions_api.Configeration;
using stock_predictions_api.Models;
using stock_predictions_api.Models.DB;

namespace stock_predictions_api.Controllers
{
    [Produces("application/json")]
    public class SettingsController : Controller
    {
        StockPredictionDBContext stockDBConext;

        public SettingsController(IConfigurationManager iconfiguration)
        {
            stockDBConext = new StockPredictionDBContext(iconfiguration); 
        }

        [HttpGet]
        [Route("api/settings")]
        public JsonResult currentSettings()
        {
            AppSettings currentAppSettings = stockDBConext.Settings.Where(settings => settings.settings_id == 1).FirstOrDefault();
            return Json(new
            {
                target_growth = currentAppSettings.target_growth
            });
        }


        [HttpPut]
        [Route("api/settings")]
        public JsonResult updateSettings([FromBody] AppSettings setting)
        {
            AppSettings currentAppSettings = stockDBConext.Settings.Where(settings => settings.settings_id == 1).FirstOrDefault();
            currentAppSettings.target_growth = setting.target_growth;
            stockDBConext.SaveChanges();
            return Json(new
            {
                saved = true
            });
        }


    }
}
