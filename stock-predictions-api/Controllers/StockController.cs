﻿using System;
using System.Globalization;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using stock_predictions_api.Classes;
using stock_predictions_api.Configeration;
using stock_predictions_api.Libraries;
using stock_predictions_api.Models;
using stock_predictions_api.Models.DB;

namespace stock_predictions_api.Controllers
{
    [Produces("application/json")]
    public class StockController : Controller
    {

        //https://www.investopedia.com/articles/basics/03/052303.asp
        //https://money.usnews.com/investing/investing-101/slideshows/how-to-pick-stocks-things-all-beginner-investors-should-know
        //https://money.usnews.com/investing/investing-101/slideshows/how-to-pick-stocks-things-all-beginner-investors-should-know?slide=9
        //https://www.investors.com/research/best-long-term-stocks/#:~:text=While%20not%20mandatory%2C%20the%20best,EPS%2C%20SMR%20and%20Composite%20Ratings.

        private readonly StockPredictionDBContext stockDBConext;
        private readonly Security securityObject;
        
        public StockController(IConfigurationManager iconfiguration)
        {
            stockDBConext = new StockPredictionDBContext(iconfiguration);
            securityObject = new Security(stockDBConext);
        }

        [HttpGet]
        [Route("api/stock")]
        public JsonResult stockList()
        {
            return Json(securityObject.List());
        }

        [HttpPost]
        [Route("api/stock")]
        public JsonResult saveStock([FromBody] StockModel stock)
        {
            return Json(securityObject.Add(stock));
        }

        [HttpDelete]
        [Route("api/stock/{stock_id}")]
        public JsonResult removeStock(int stock_id)
        {
            var securityObject = new Security(stockDBConext);
            securityObject.Remove(stock_id);
            return Json(new { saved = true });
        }

        [HttpPut]
        [Route("api/stock/{stock_id}")]
        public JsonResult updateStock(string stock_id)
        {
            var securityObject = new Security(stockDBConext);
            return Json(securityObject.Update(Convert.ToInt32(stock_id)));
        }

        [HttpGet]
        [Route("api/stock/{stock_id}")]
        public JsonResult getSecurity(string stock_id)
        {
            var securityInfo = new Security(stockDBConext);
            return Json(securityInfo.Get(Convert.ToInt32(stock_id)));
        }
    }
}
