﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using stock_predictions_api.Classes;
using stock_predictions_api.Configeration;
using stock_predictions_api.Libraries;
using stock_predictions_api.Models;
using stock_predictions_api.Models.DB;

namespace stock_predictions_api.Controllers
{
    [Produces("application/json")]
    [Route("api/trades")]
    public class TradeController : Controller
    {
        private readonly StockPredictionDBContext _dbContext;
        private readonly Trade _trade;

        public TradeController(IConfigurationManager iconfiguration)
        {
            _dbContext = new StockPredictionDBContext(iconfiguration);
            _trade = new Trade(_dbContext);
        }

        [HttpGet]
        public JsonResult List(string stock_id)
        {
            return Json(_trade.List());
        }

        [HttpGet("{security}")]
        public JsonResult ListByStock(string security)
        {
            return Json(_trade.ListBySecurity(security));
        }

        [HttpGet("{id}/data")]
        public JsonResult TradeData(int id)
        {
            StockTradeHistoryModel trade = _trade.Get(id);
            string trade_info_file_path = String.Format(@"Content/{0}", trade.trade_json);
            return Json(new {
                security = trade.security,
                trade_purchase_date = trade.trade_purchase_date,
                trade_selling_date = trade.trade_selling_date,
                data = _trade.TradeDetails(trade_info_file_path)
            });
        }

        [HttpPut("{id}/data")]
        public JsonResult UpdateTradeData(int id, [FromBody] JsonTradeData newFundamentals)
        {
            _trade.UpdateTradeFundamentals(id, newFundamentals);
            return Json(newFundamentals.data);
        }

        //[HttpGet("view/{id}")]
        //public JsonResult GetById(int id)
        //{
        //    StockTradeHistoryModel trade = _trade.Get(id);



        //    return Json(_trade.ListBySecurity(security));
        //}

        [HttpPost]
        public JsonResult NewTrade([FromBody] StockTradeHistoryModel trade)
        {
            return Json(_trade.Add(trade));
        }

        [HttpPut("{trade_id}")]
        public JsonResult updateTrade(int trade_id, [FromBody] StockTradeHistoryModel trade)
        {
            return Json(_trade.Update(trade_id, trade));
        }


    }
}
