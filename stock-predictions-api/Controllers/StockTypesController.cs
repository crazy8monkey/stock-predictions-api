﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using stock_predictions_api.Classes;
using stock_predictions_api.Configeration;
using stock_predictions_api.Models.DB;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace stock_predictions_api.Controllers
{
    [Produces("application/json")]
    public class StockTypesController : Controller
    {
        private readonly StockPredictionDBContext stockDBConext;
        private readonly StockType _stockType;
        private readonly StockTypeData _stockTypeData;

        public StockTypesController(IConfigurationManager iconfiguration)
        {
            stockDBConext = new StockPredictionDBContext(iconfiguration);
            _stockType = new StockType(stockDBConext);
            _stockTypeData = new StockTypeData(stockDBConext);
        }

        [HttpGet]
        [Route("api/stock-types")]
        public JsonResult stockTypes()
        {
            return Json(_stockType.List());
        }

        [HttpGet]
        [Route("api/stock-types/{id}")]
        public JsonResult getStockType(int id)
        {
            return Json(_stockType.Get(id));
        }

        [HttpPost]
        [Route("api/stock-types")]
        public JsonResult NewStockType([FromBody] AppSettingStockTypeModel type)
        {
            return Json(_stockType.Add(type));
        }

        [HttpGet("api/stock-types/{id}/data")]
        public JsonResult getStockTypeData(int id)
        {
            return Json(_stockTypeData.List(id));
        }

        [HttpPost("api/stock-types/{id}")]
        public JsonResult saveData(int id, [FromBody] List<AppSettingStockTypeDataModel> data)
        {
            return Json(_stockTypeData.UpdateManual(id, data));
        }
    }
}
