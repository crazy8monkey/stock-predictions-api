﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using stock_predictions_api.Libraries;
using stock_predictions_api.Models;
using Newtonsoft.Json;
using Microsoft.ML;
using System.Collections.Generic;
using System.Linq;
using stock_predictions_api.Configeration;
using stock_predictions_api.Classes;
using stock_predictions_api.Models.DB;

namespace stock_predictions_api.Controllers
{
    [Produces("application/json")]
    [Route("api/cron")]
    public class CronController : Controller
    {
        private readonly MachineLearningHandler machineLearningHandler;
        private readonly StockPredictionDBContext _dbContext;
        private readonly Trade _trades;
        private readonly Screener _screener;
        private readonly FinvizHandler finvizHandler;

        public CronController(IConfigurationManager iconfiguration)
        {
            _dbContext = new StockPredictionDBContext(iconfiguration);
            machineLearningHandler = new MachineLearningHandler();
            _trades = new Trade(_dbContext);
            _screener = new Screener(_dbContext);
            finvizHandler = new FinvizHandler();
        }

        [Route("trade-data")]
        [HttpGet]
        public JsonResult getTradeData()
        {
            List<TradeDataLearn> captured_data = _trades.TradeData();
            return Json(machineLearningHandler.GenerateTradingData(captured_data));
        }

        [HttpGet("train-data")]
        public JsonResult trainData()
        {
            List<TradeDataLearn> captured_data = _trades.TradeData();
            IEnumerable<TradeDataLearn> trainData = machineLearningHandler.GenerateTradingData(captured_data);
            ITransformer pipe_line = machineLearningHandler.BuildTrainingPipeline(trainData);
            machineLearningHandler.PredictData(pipe_line, captured_data);
            //return Json(new { success = true });
            return Json(pipe_line);
        }

        [HttpGet("screener/{id}")]
        public JsonResult stockStreener(int id)
        {   
            List<AppSettingStockTypeDataModel> fundamentals = _screener.getFundamentials(id);

            finvizHandler.getSecurityRequest(fundamentals);

            return Json(new { success = true });
        }
    }
}
