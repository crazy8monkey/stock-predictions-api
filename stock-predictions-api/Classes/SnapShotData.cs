﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;
using Newtonsoft.Json;
using stock_predictions_api.Libraries;
using stock_predictions_api.Models;
using stock_predictions_api.Models.DB;
using stock_predictions_api.Models.IEX;

namespace stock_predictions_api.Classes
{
    public class SnapShotData
    {
        public string security { get; set; }

        private readonly StockPredictionDBContext _dbContext;
        private readonly IEXHandler _iexHandler;

        public SnapShotData(StockPredictionDBContext dBContext)
        {
            _dbContext = dBContext;
            _iexHandler = new IEXHandler(_dbContext.GetAppSettings());
        }

        public List<SnapShotDataModel> List(string security)
        {
            return _dbContext.SnapShotData.Where(data => data.security == security).ToList();
        }

        public SnapShotDataModel Get(SnapShotDataModel snapShot)
        {
            return _dbContext.SnapShotData.Where(data => data.security == snapShot.security)
                                          .Where(data => data.label == snapShot.label).FirstOrDefault();

        }

        public SnapShotDataModel GetByLabel(string security, string label)
        {
            return _dbContext.SnapShotData.Where(data => data.security == security)
                                          .Where(data => data.label == label).FirstOrDefault();

        }


        public SnapShotDataModel Add(SnapShotDataModel snapShot)
        {
            _dbContext.SnapShotData.Add(snapShot);
            _dbContext.SaveChanges();
            return snapShot;
        }

        public void CreateData(string avgVol)
        {
            DateTime newTime = DateTime.UtcNow;
            List<SnapShotDataModel> snap_shot_data = new List<SnapShotDataModel>();
            var security_snapshot = new ExpandoObject() as IDictionary<string, object>;
            security_snapshot.Add("Avg Volume", avgVol);
            security_snapshot.Add("Price To Earnings", "");
            security_snapshot.Add("Price To Sales", "");
            security_snapshot.Add("Price To Book", "");
            security_snapshot.Add("Price Cash Per Share", "");
            security_snapshot.Add("Return On Equity", "");
            security_snapshot.Add("Debt To Equity", "");
            security_snapshot.Add("Earnings Per Share", "");

            foreach (KeyValuePair<string, object> kvp in security_snapshot)
            {
                snap_shot_data.Add(new SnapShotDataModel
                {
                    security  = security,
                    label = kvp.Key,
                    data = ((string)(kvp.Key == "Avg Volume" ? kvp.Value : "")),
                    updated_date = newTime,
                    update_type = ((string)(kvp.Key == "Avg Volume" ? "API Update" : ""))
                });
            }

            _dbContext.SnapShotData.AddRange(snap_shot_data);
            _dbContext.SaveChanges();

        }

        public void UpdateData(string avgVol)
        {
            DateTime newTime = DateTime.UtcNow;
            AdvancedStatsModel iexFundamental = grabIEXFundamentals();
            var current_snap_shot_data = _dbContext.SnapShotData.Where(data => data.security == security).ToList();
            foreach (SnapShotDataModel stockInfo in current_snap_shot_data)
            {
                switch (stockInfo.label)
                {
                    case "Avg Volume":
                        {
                            stockInfo.data = avgVol;
                            stockInfo.updated_date = newTime;
                            stockInfo.update_type = "API Update";
                            break;
                        }
                    case "Price To Earnings":
                        {
                            if(iexFundamental.pegRatio != null)
                            {
                                stockInfo.data = Convert.ToString(Math.Round((decimal)iexFundamental.pegRatio, 2));
                                stockInfo.updated_date = newTime;
                                stockInfo.update_type = "API Update";
                            } 
                            break;
                        }
                    case "Price To Sales":
                        {
                            if(iexFundamental.priceToSales != null)
                            {
                                stockInfo.data = Convert.ToString(Math.Round((decimal)iexFundamental.priceToSales, 2));
                                stockInfo.updated_date = newTime;
                                stockInfo.update_type = "API Update";
                            }
                            break;
                        }
                    case "Price To Book":
                        {
                            if(iexFundamental.priceToBook != null)
                            {
                                stockInfo.data = Convert.ToString(Math.Round((decimal)iexFundamental.priceToBook, 2));
                                stockInfo.updated_date = newTime;
                                stockInfo.update_type = "API Update";
                            }
                            break;
                        }
                    /* this will be dealt with later 
                    case "Price Cash Per Share":
                        {
                            break;
                        }

                        case "Return On Equity":
                        {
                            break;
                        }

                     
                     */
                    case "Debt To Equity":
                        {
                            if(iexFundamental.debtToEquity != null)
                            {
                                stockInfo.data = Convert.ToString(Math.Round((decimal)iexFundamental.debtToEquity, 2));
                                stockInfo.updated_date = newTime;
                                stockInfo.update_type = "API Update";
                            }
                            break;
                        }
                        /*
                            case "Earnings Per Share":
                            {
                                //Do some stuff
                                break;
                            } 
                         */

                }
            }
        }

        public List<SnapShotDataModel> AddMultiple(List<SnapShotDataModel> data)
        {
            _dbContext.SnapShotData.AddRange(data);
            _dbContext.SaveChanges();
            return data;
        }

        public List<SnapShotDataModel> UpdateMultiple(string security, List<SnapShotDataModel> data)
        {
            List<SnapShotDataModel> add_new_snapshot = new List<SnapShotDataModel>();
            var currentSavedData = _dbContext.SnapShotData.Where(data => data.security == security).ToList();
            foreach (SnapShotDataModel snapData in data)
            {
                var currentSnapShotData = currentSavedData.Find(snap_shot => (snap_shot.label == snapData.label) && (snap_shot.security == security));
                if(currentSnapShotData != null)
                {
                    currentSnapShotData.data = snapData.data;
                    currentSnapShotData.updated_date = snapData.updated_date;
                    currentSnapShotData.update_type = snapData.update_type;
                }
                else
                {
                    add_new_snapshot.Add(snapData);
                }
            }

            _dbContext.SnapShotData.AddRange(add_new_snapshot);
            _dbContext.SaveChanges();
            return data;
        }


        public List<SnapShotDataModel> UpdateManual(string security, List<SnapShotDataModel> data)
        {
            DateTime newTime = DateTime.UtcNow;
            var currentSavedData = _dbContext.SnapShotData.Where(data => data.security == security).ToList();
            foreach (SnapShotDataModel snapData in data)
            {
                var currentSnapShotData = currentSavedData.Find(snap_shot => (snap_shot.label == snapData.label) && (snap_shot.security == security));
                if (currentSnapShotData != null)
                {
                    currentSnapShotData.data = snapData.data;
                    currentSnapShotData.updated_date = newTime;
                }
                
            }

            _dbContext.SaveChanges();
            return data;
        }


        private AdvancedStatsModel grabIEXFundamentals()
        {
            _iexHandler.security = security;
            return _iexHandler.GetAdvanceStats();
        }

    }
}
