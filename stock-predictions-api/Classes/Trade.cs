﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Reflection;
using Microsoft.ML;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using stock_predictions_api.Libraries;
using stock_predictions_api.Models;
using stock_predictions_api.Models.DB;

namespace stock_predictions_api.Classes
{
    public class Trade
    {
        private readonly StockPredictionDBContext _dbContext;
        private readonly SnapShotData _snapShotData;
        private readonly Security _security;

        public Trade(StockPredictionDBContext dbContext)
        {
            _dbContext = dbContext;
            _snapShotData = new SnapShotData(dbContext);
            _security = new Security(_dbContext);
        }

        public List<StockTradeHistoryModel> ListBySecurity(string security)
        {
            return _dbContext.TradingHistory.Where(trade => trade.security == security).ToList();
        }

        public List<StockTradeHistoryModel> List()
        {
            return _dbContext.TradingHistory.ToList();
        }

        public StockTradeHistoryModel Get(int trade_id)
        {
            return _dbContext.TradingHistory.Where(trade => trade.trade_id == trade_id).FirstOrDefault();
        }

        public StockTradeHistoryModel Add(StockTradeHistoryModel trade)
        {
            Console.WriteLine("security test => {0}", trade.security);
            StockTradeHistoryModel newTrade = new StockTradeHistoryModel();
            newTrade.security = trade.security;
            newTrade.trade_purchase_date = trade.trade_purchase_date;
            newTrade.trade_selling_date = (trade.trade_selling_date != null ? trade.trade_selling_date : null);
            newTrade.trade_sector = trade.trade_sector;
            newTrade.trade_purchase_price = Convert.ToDecimal(trade.trade_purchase_price);
            _dbContext.TradingHistory.Add(trade);
            _dbContext.SaveChanges();
            newTrade.trade_id = newTrade.trade_id;
            return trade;
        }

        public StockTradeHistoryModel Update(int trade_id, StockTradeHistoryModel trade)
        {
            
            var currentSecurity = _security.GetByShortCode(trade.security);
            var currentTrade = _dbContext.TradingHistory.Where(trade => trade.security == trade.security)
                                                        .Where(trade => trade.trade_id == trade_id).FirstOrDefault();
            string tradeJsonFile = String.Format("{0}_trade_{1:yyyy-MM-dd}_{2:yyyy-MM-dd}.json", trade.security, currentTrade.trade_purchase_date, trade.trade_selling_date);
            SaveTrade(tradeJsonFile, trade.security);
            currentTrade.market_cap = _security.GenerateMarketCap((long)currentSecurity.market_cap);
            currentTrade.trade_selling_date = trade.trade_selling_date;
            currentTrade.trade_selling_price = trade.trade_selling_price;
            currentTrade.trade_sector = trade.trade_sector;
            currentTrade.trade_days_length = CalculateTradeDays(currentTrade.trade_purchase_date, Convert.ToDateTime(trade.trade_selling_date));
            currentTrade.trade_return = CalculateReturn(Convert.ToDecimal(currentTrade.trade_purchase_price), Convert.ToDecimal(trade.trade_selling_price));
            currentTrade.trade_json = tradeJsonFile;
            _dbContext.SaveChanges();
            return currentTrade;
        }

        public ExpandoObject TradeDetails(string file_path)
        {
            dynamic tradeDetails = new ExpandoObject();
            string trade_record = File.ReadAllText(file_path);
            return JsonConvert.DeserializeObject<ExpandoObject>(trade_record);
        }

        public void UpdateTradeFundamentals(int id, JsonTradeData trade)
        {
            StockTradeHistoryModel currentTrade = Get(id);
            string snapShotJson = JsonConvert.SerializeObject(trade.data);
            System.IO.File.WriteAllText(String.Format("Content/{0}", currentTrade.trade_json), snapShotJson);
        }

        public List<TradeDataLearn> TradeData()
        {
            var tradingHistory = _dbContext.TradingHistory.Where(trade => trade.trade_selling_date != null)
                                                          .Where(trade => trade.trade_return >= 20).ToList();
            List<TradeDataLearn> tradeData = new List<TradeDataLearn>();
            foreach (StockTradeHistoryModel tradeInfo in tradingHistory)
            {
                dynamic tradeDetails = new ExpandoObject();
                string trade_info_file_path = String.Format(@"Content/{0}", tradeInfo.trade_json);
                string trade_record = File.ReadAllText(trade_info_file_path);
                tradeDetails = JsonConvert.DeserializeObject<ExpandoObject>(trade_record);

                JObject parsedJson = JObject.Parse(trade_record);
                foreach (JProperty property in parsedJson.Properties())
                {
                    //if(property.Name == "Avg Volume")
                    //{
                        TradeDataLearn trainData = new TradeDataLearn();
                        trainData.ReturnPercentage = (float)tradeInfo.trade_return;
                        trainData.SnapShotLabel = property.Name;
                        trainData.SnapShotValue = (float)Convert.ToDouble(property.Value);
                        tradeData.Add(trainData);
                    //}
                    
                }


            }
            return tradeData;
        }

        //private methods
        private decimal CalculateReturn(decimal purchase_price, decimal selling_price)
        {
            decimal growth = selling_price - purchase_price;
            decimal percentageValue = ((growth / purchase_price) * 100);
            return Math.Round(percentageValue, 2);
        }

        private int CalculateTradeDays(DateTime start_date, DateTime end_date)
        {
            return (end_date.Date - start_date.Date).Days;
        }

        private void SaveTrade(string file_name, string security)
        {
            List<SnapShotDataModel> snap_shot_data = _snapShotData.List(security);
            var trade_snap_shot = new ExpandoObject() as IDictionary<string, object>;
            foreach (SnapShotDataModel snapShot in snap_shot_data)
            {   
                if(snapShot.label == "Avg Volume")
                {
                    trade_snap_shot.Add(snapShot.label, Convert.ToInt64(snapShot.data));
                } else
                {
                    if(snapShot.data.ToString() == "")
                    {
                        trade_snap_shot.Add(snapShot.label, null);
                    } else
                    {
                        trade_snap_shot.Add(snapShot.label, Convert.ToDecimal(snapShot.data.ToString()));
                    }
                }
            }
            string snapShotJson = JsonConvert.SerializeObject(trade_snap_shot);
            System.IO.File.WriteAllText(String.Format("Content/{0}", file_name), snapShotJson);
        }
    }
}
