﻿using System;
using System.Collections.Generic;
using System.Linq;
using stock_predictions_api.Models.DB;

namespace stock_predictions_api.Classes
{
    public class StockTypeData
    {
        private readonly StockPredictionDBContext _dbContext;

        public StockTypeData(StockPredictionDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<AppSettingStockTypeDataModel> List(int id)
        {
            return _dbContext.SettingStockTypesData.Where(field => field.stock_type_id == id).ToList();
        }

        public List<AppSettingStockTypeDataModel> UpdateManual(int id, List<AppSettingStockTypeDataModel> data)
        {
            var currentSavedData = _dbContext.SettingStockTypesData.Where(field => field.stock_type_id == id).ToList();
            List<AppSettingStockTypeDataModel> typeData = new List<AppSettingStockTypeDataModel>();
            foreach (AppSettingStockTypeDataModel data_single in data)
            {
                var currentField = currentSavedData.Find(field => (field.attribute == data_single.attribute));
                if(currentField != null)
                {

                }
                typeData.Add(new AppSettingStockTypeDataModel
                {
                    stock_type_id = id,
                    attribute = data_single.attribute,
                    data = data_single.data,
                    greater_lower = data_single.greater_lower
                });
            }

            _dbContext.SettingStockTypesData.AddRange(typeData);
            _dbContext.SaveChanges();
            return typeData;

            
            //foreach (SnapShotDataModel snapData in data)
            //{
            //    var currentSnapShotData = currentSavedData.Find(snap_shot => (snap_shot.label == snapData.label) && (snap_shot.security == security));
            //    if (currentSnapShotData != null)
            //    {
            //        currentSnapShotData.data = snapData.data;
            //        currentSnapShotData.updated_date = newTime;
            //    }

            //}

            //_dbContext.SaveChanges();
            //return data;
        }
    }
}
