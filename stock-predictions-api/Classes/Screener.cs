﻿using System;
using System.Collections.Generic;
using System.Linq;
using stock_predictions_api.Models.DB;

namespace stock_predictions_api.Classes
{
    public class Screener
    {
        private readonly StockPredictionDBContext _dbContext;

        public Screener(StockPredictionDBContext dBContext)
        {
            _dbContext = dBContext;
        }

        public List<AppSettingStockTypeDataModel> getFundamentials(int id)
        {
            return _dbContext.SettingStockTypesData.Where(attr => attr.stock_type_id == id).ToList();
        }
    }
}
