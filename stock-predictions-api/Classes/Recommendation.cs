﻿using System;
using System.Collections.Generic;
using System.Linq;
using stock_predictions_api.Models;
using stock_predictions_api.Models.DB;

namespace stock_predictions_api.Classes
{
    public class Recommendation
    {
        private readonly StockPredictionDBContext _dbContext;

        public Recommendation(StockPredictionDBContext dBContext)
        {
            _dbContext = dBContext;
        }

        public List<RecommendationModel> UpdateRecommendations(List<RecommendationModel> data)
        {
            var currentRecommendations = _dbContext.Recommendation.ToList();
            foreach (RecommendationModel recommendation in currentRecommendations)
            {
                //RecommendationModel recObject = currentRecommendations.Find(
                //        rec => (rec.market_cap == recommendation.market_cap) &&
                //               (rec.sector == recommendation.sector) &&
                //               (rec.time_frame == recommendation.time_frame) &&
                //               (rec.label == recommendation.label)
                //    );
            //    if (snapData.label == currentSnapShotData.label)
            //    {
            //        snapData.data = currentSnapShotData.data;
            //    }
            }
            //_dbContext.SaveChanges();
            return data;
        }
    }
}
