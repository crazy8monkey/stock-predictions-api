﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.ML;
using Newtonsoft.Json;
using stock_predictions_api.Libraries;
using stock_predictions_api.Models.DB;
using stock_predictions_api.Models;
using stock_predictions_api.Models.IEX;
using stock_predictions_api.Models.FinancialPrep;

namespace stock_predictions_api.Classes
{
    public class Security
    {
        private AppSettings appSettings = new AppSettings();

        private readonly IEXHandler iexHandler;
        private readonly FinancialPrepHandler financialPrepHandler;
        private readonly StockPredictionDBContext _dbContext;
        private readonly SnapShotData snapShotData;

        public Security(StockPredictionDBContext dBContext)
        {
            _dbContext = dBContext;
            financialPrepHandler = new FinancialPrepHandler(_dbContext.GetAppSettings());
            iexHandler = new IEXHandler(_dbContext.GetAppSettings());
            snapShotData = new SnapShotData(_dbContext);

        }

        public List<StockModel> List()
        {
            return _dbContext.Security.ToList();
        }

        public StockModel Get(int stock_id)
        {
            return _dbContext.Security.Where(stock => stock.id == stock_id).FirstOrDefault();
        }

        public StockModel GetByShortCode(string short_code)
        {
            return _dbContext.Security.Where(stock => stock.short_code == short_code).FirstOrDefault();
        }

        public StockModel Add(StockModel stock)
        {
            List<CompanyInfoModel> company_data = CompanyData(stock.short_code);
            List<HistoricalChartModel> history = GetNineMonthHistory(stock.short_code);

            snapShotData.security = stock.short_code;
            snapShotData.CreateData(Convert.ToString(company_data[0].volAvg));   

            StockModel newStock = new StockModel();
            newStock.short_code = stock.short_code;
            newStock.current_price = Convert.ToDecimal(company_data[0].price);
            newStock.sector = company_data[0].sector;
            newStock.three_month_close = history[91].close;
            newStock.six_month_close = history[182].close;
            newStock.nine_month_close = history[history.Count - 1].close;
            newStock.market_cap = company_data[0].mktCap;
            _dbContext.Security.Add(newStock);
            _dbContext.SaveChanges();   
            return newStock;
        }

        public StockModel Update(int id)
        {
            var security = _dbContext.Security.Where(stock => stock.id == id).FirstOrDefault();
            List<CompanyInfoModel> company_data = CompanyData(security.short_code);
            List<HistoricalChartModel> history = GetNineMonthHistory(security.short_code);

            snapShotData.security = security.short_code;
            snapShotData.UpdateData(Convert.ToString(company_data[0].volAvg));
            
            history.Reverse();
            security.last_modified = DateTime.UtcNow;
            security.current_price = Convert.ToDecimal(company_data[0].price);
            security.sector = company_data[0].sector;
            security.three_month_close = history[91].close;
            security.six_month_close = history[182].close;
            security.nine_month_close = history[history.Count - 1].close;
            security.market_cap = company_data[0].mktCap;
            _dbContext.SaveChanges();
           
            return security;
        }

        public void Remove(int stock_id)
        {
            var security = _dbContext.Security.Where(stock => stock.id == stock_id).FirstOrDefault();
            _dbContext.Security.Remove(security);
            _dbContext.SaveChanges();
        }

        public FinancialPrepHandler getFinancialPrepApi()
        {
            return financialPrepHandler;
        }

        public IEXHandler getIEXHandler()
        {
            return iexHandler;
        }

        public StockPredictionDBContext GetDBContext()
        {
            return _dbContext;
        }

        public string GenerateMarketCap(long market_cap)
        {
            /*
             nano caps = below $50 million
             micro cap = $50 million to $300 million
             small cap = $300 million to $2 billion
             mid cap = $2 billion and $10 billion
             large cap = $10 billion or more
             */

            string mid_cap = "";
            if (50000000 >= market_cap)
            {
                mid_cap = "Nano Cap";
            }
            else if (50000001 <= market_cap && market_cap <= 300000000)
            {
                mid_cap = "Micro Cap";
            }
            else if (300000001 <= market_cap && market_cap <= 2000000000)
            {
                mid_cap = "Small Cap";
            }
            else if (2000000001 <= market_cap && market_cap <= 10000000000)
            {
                mid_cap = "Mid Cap";
            }
            else
            {
                mid_cap = "Large Cap";
            }

            return mid_cap;
        }

        //public 


        //calculate quantitative data
        /*
         * https://blog.quantinsti.com/quantitative-value-investing-strategy-python/
         * https://seekingalpha.com/article/2260803-investing-for-beginners-with-benjamin-graham
         * https://www.serenitystocks.com/article/investing-beginners-benjamin-graham
         * 
            1. low valuations
            2. demonstrated earning power
            3. good returns while employing little to no debt
            4. management having ownership
         
         */

        //private methods
        private List<CompanyInfoModel> CompanyData(string security)
        {
            financialPrepHandler.security = security;
            return financialPrepHandler.GetCompanyProfile();
        }

        private List<HistoricalChartModel> GetNineMonthHistory(string security)
        {
            iexHandler.security = security;
            return iexHandler.GetNineMonthChart();
        }

        private List<KeyMetricsModel> KeyMetrics(string security)
        {
            financialPrepHandler.security = security;
            return financialPrepHandler.KeyMetrics();
        }
    }
}
