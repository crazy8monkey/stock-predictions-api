﻿using System;
using System.Collections.Generic;
using System.Linq;
using stock_predictions_api.Models.DB;

namespace stock_predictions_api.Classes
{
    public class StockType
    {
        private readonly StockPredictionDBContext _dbContext;

        public StockType(StockPredictionDBContext dbContext)
        {
            _dbContext = dbContext;
        }

        public List<AppSettingStockTypeModel> List()
        {
            return _dbContext.SettingStockTypes.ToList();
        }

        public AppSettingStockTypeModel Add(AppSettingStockTypeModel type)
        {
            _dbContext.SettingStockTypes.Add(type);
            _dbContext.SaveChanges();
            return type;
        }

        public AppSettingStockTypeModel Get(int id)
        {
            return _dbContext.SettingStockTypes.Where(type => type.id == id).FirstOrDefault();
        }

    }
}
