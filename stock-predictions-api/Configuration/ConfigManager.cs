﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Configuration;

namespace stock_predictions_api.Configeration
{
    public class ConfigManager : IConfigurationManager
    {
        private readonly IConfiguration _configuration;
        public ConfigManager(IConfiguration configuration)
        {
            this._configuration = configuration;
        }

        public string DBConnection
        {
            get
            {
                return this._configuration["ConnectionStrings:DBConnection"];
            }
        }

        public string FinancialPrepUrl
        {
            get {
                return this._configuration["FinancialModel:url"];
            }
        }

        public string FinancialPrepKey
        {
            get
            {
                return this._configuration["FinancialModel:apiKey"];
            }
        }

        public string IEXUrl
        {
            get
            {
                return this._configuration["IEXCloud:url"];
            }
        }

        public string IEXKey
        {
            get
            {
                return this._configuration["IEXCloud:token"];
            }
        }

        
        public string AlphaVantageUrl
        {
            get
            {
                return this._configuration["AlphaVantage:url"];
            }
        }

        public string AlphaVantageKey
        {
            get
            {
                return this._configuration["AlphaVantage:apiKey"];
            }
        }

        public IConfigurationSection GetConfigurationSection(string key)
        {
            return this._configuration.GetSection(key);
        }

        public string GetConnectionString(string connectionName)
        {
            throw new NotImplementedException();
        }
    }
}