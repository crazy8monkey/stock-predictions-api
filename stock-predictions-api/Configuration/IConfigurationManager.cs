﻿using System;
using Microsoft.Extensions.Configuration;

namespace stock_predictions_api.Configeration
{
    public interface IConfigurationManager
    {

        string DBConnection { get; }

        //financial api
        string FinancialPrepUrl { get; }
        string FinancialPrepKey { get; }

        //iex api
        string IEXUrl { get; }
        string IEXKey { get; }

        //alpha vantgate
        string AlphaVantageUrl { get; }
        string AlphaVantageKey { get; }


        string GetConnectionString(string connectionName);

        IConfigurationSection GetConfigurationSection(string Key);
    }
}
