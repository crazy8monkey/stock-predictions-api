﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.ML;
using Microsoft.ML.Data;
using Microsoft.ML.Trainers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using stock_predictions_api.Classes;
using stock_predictions_api.Models;
using stock_predictions_api.Models.DB;

namespace stock_predictions_api.Libraries
{
    public class MachineLearningHandler
    {
        /*
         * https://docs.microsoft.com/en-us/dotnet/api/microsoft.ml.normalizationcatalog.normalizeminmax?view=ml-dotnet
         * https://xamlbrewer.wordpress.com/2019/04/05/machine-learning-with-ml-net-in-uwp-recommendation/
         * https://medium.com/machinelearningadvantage/build-a-movie-recommender-using-c-and-ml-net-machine-learning-d6175ae13bc9
         * https://github.com/XamlBrewer/UWP-MachineLearning-Sample
         * https://towardsdatascience.com/predicting-a-house-price-using-ml-net-6555ff3caeb
         * https://docs.microsoft.com/en-us/dotnet/machine-learning/tutorials/predict-prices
         * https://github.com/dotnet/machinelearning/issues/3883
         * https://github.com/dotnet/machinelearning/blob/master/docs/code/MlNetCookBook.md
         * https://rubikscode.net/2019/02/18/ultimate-guide-to-machine-learning-with-ml-net/
         * https://www.youtube.com/watch?v=4AY4IYYf7F4
         * https://github.com/XamlBrewer/UWP-MachineLearning-Sample/tree/4f6efe213f355707721a694f327c1d6fee48d7e7
         * https://docs.microsoft.com/en-us/dotnet/api/microsoft.ml.categoricalcatalog.onehotencoding?view=ml-dotnet
         * https://docs.microsoft.com/en-us/dotnet/api/microsoft.ml.textcatalog.featurizetext?view=ml-dotnet
         * https://docs.microsoft.com/en-us/dotnet/api/microsoft.ml.normalizationcatalog.normalizeminmax?view=ml-dotnet
         * https://github.com/dotnet/machinelearning-samples/blob/master/samples/csharp/getting-started/MatrixFactorization_MovieRecommendation/MovieRecommendation/Program.cs
         * https://xamlbrewer.wordpress.com/2019/04/23/machine-learning-with-ml-net-in-uwp-field-aware-factorization-machine/
         * https://github.com/habib-developer/Price-Prediction
         */


        /*
         1. create list of trading data
         2. convert trading data to the correct format, to floats
            - https://docs.microsoft.com/en-us/dotnet/api/microsoft.ml.conversionsextensionscatalog.converttype?view=ml-dotnet
         3. create pipeline with converted trading data
         
         */

        private readonly MLContext _mlcontext = new MLContext();
        
        //public MachineLearningHandler(StockPredictionDBContext dbContext)
        //{
        //}

        public IEnumerable<TradeDataLearn> GenerateTradingData(List<TradeDataLearn> trade_data)
        {
            return _mlcontext.Data.CreateEnumerable<TradeDataLearn>(_mlcontext.Data.LoadFromEnumerable(trade_data), true);
        }

        public ITransformer BuildTrainingPipeline(IEnumerable<TradeDataLearn> train_data)
        {
            IDataView trainInfoDataView = _mlcontext.Data.LoadFromEnumerable(train_data);

            Console.WriteLine("=============== Training the model ===============");
            var dataProcessingPipeline = _mlcontext.Transforms.CopyColumns(outputColumnName: "Label", inputColumnName: "SnapShotValue")
                                         .Append(_mlcontext.Transforms.Categorical.OneHotEncoding("SnapShotLabelEncoded", "SnapShotLabel"))
                                         .Append(_mlcontext.Transforms.NormalizeMeanVariance("SnapShotLabelEncoded"))
                                         .Append(_mlcontext.Transforms.Concatenate("Features", new[] { "SnapShotLabelEncoded", "ReturnPercentage" }))
                                         .Append(_mlcontext.Regression.Trainers.Sdca());
                                         //.Append(_mlcontext.Regression.Trainers.FastTree());
                                         //.Append(_mlcontext.Regression.Trainers.FastTree(labelColumnName: "Label", featureColumnName: "Features"));
                                         //.Append(_mlcontext.Regression.Trainers.Sdca(labelColumnName: "Label", featureColumnName: "Features"));

            var model = dataProcessingPipeline.Fit(trainInfoDataView);
            return model;
        }


        public List<RecommendationModel> PredictData(ITransformer model, List<TradeDataLearn> trade_data)
        {
            List<RecommendationModel> listOfRecommendations = new List<RecommendationModel>();
            var predictionengine = _mlcontext.Model.CreatePredictionEngine<TradeDataLearn, TradeDataPrediction>(model);

            foreach (TradeDataLearn trade in trade_data)
            {

                var snapshotPrediction = predictionengine.Predict(new TradeDataLearn
                {
                    ReturnPercentage = trade.ReturnPercentage,
                    SnapShotLabel = trade.SnapShotLabel,
                    SnapShotValue = 0
                });

                Console.WriteLine($"Return Percentage: {trade.ReturnPercentage}%");
                Console.WriteLine($"Snapshot Label: {trade.SnapShotLabel}");
                Console.WriteLine($"Trade Snapshot Value: {trade.SnapShotValue}");
                Console.WriteLine($"Snapshot Prediction: {snapshotPrediction.SnapShotValue:0.####}");
                Console.WriteLine($"Snapshot Prediction: {0}", Sigmoid(snapshotPrediction.SnapShotValue));
                Console.WriteLine("==========================");
            }
            return listOfRecommendations;
        }

        public static float Sigmoid(float x)
        {
            return (float)(100 / (1 + Math.Exp(-x)));
        }

    }
}
