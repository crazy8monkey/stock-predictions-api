﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using stock_predictions_api.Configeration;
using stock_predictions_api.Models;
using stock_predictions_api.Models.FinancialPrep;

namespace stock_predictions_api.Libraries
{
    public class FinancialPrepHandler
    {
        public string security { get; set; }

        private readonly string _apiUrl;
        private readonly string _apiToken;

        public FinancialPrepHandler(IConfigurationManager iconfiguration)
        {
            _apiUrl = iconfiguration.FinancialPrepUrl;
            _apiToken = iconfiguration.FinancialPrepKey;
        }

        //api/v3/profile/
        public List<CompanyInfoModel> GetCompanyProfile()
        {
            var monthChartUrl = String.Format("{0}api/v3/profile/{1}?apikey={2}", _apiUrl, security, _apiToken);
            string jsonResponse = WriteToFile(security, monthChartUrl);

            return JsonConvert.DeserializeObject<List<CompanyInfoModel>>(jsonResponse);
        }

        //api/v3/key-metrics/{stock}?period=quarter&limit=130&apikey=demo
        public List<KeyMetricsModel> KeyMetrics()
        {
            var monthChartUrl = String.Format("{0}api/v3/key-metrics/{1}?period=quarter&limit=130&apikey={2}", _apiUrl, security, _apiToken);
            string jsonResponse = WriteToFile(security, monthChartUrl);

            return JsonConvert.DeserializeObject<List<KeyMetricsModel>>(jsonResponse);
        }

        private string WriteToFile(string security, string url)
        {
            var apiRequest = new HttpRequestMessage(HttpMethod.Get, url);
            using (HttpClient client = new HttpClient())
            {
                return (new WebClient()).DownloadString(url);
            }
        }
    }
}
