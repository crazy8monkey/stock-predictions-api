﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using stock_predictions_api.Configeration;
using stock_predictions_api.Models;
using stock_predictions_api.Models.IEX;

namespace stock_predictions_api.Libraries
{
    public class IEXHandler
    {
        public string security { get; set; }

        private readonly string _apiUrl;
        private readonly string _apiToken;
        private HttpClient _client;

        public IEXHandler(IConfigurationManager iconfiguration)
        {
            _apiUrl = iconfiguration.IEXUrl;
            _apiToken = iconfiguration.IEXKey;
            _client = new HttpClient();
        }

        //stock/{security}/chart/9m
        public List<HistoricalChartModel> GetNineMonthChart()
        {
            var monthChartUrl = String.Format("{0}/stock/{1}/chart/9m?token={2}", _apiUrl, security, _apiToken);
            var jsonResponse = GetResponse(security, monthChartUrl);

            List<HistoricalChartModel> historyData = JsonConvert.DeserializeObject<List<HistoricalChartModel>>(jsonResponse);
            return historyData;
        }

        //stock/{security}/company
        //public void GetCompanyInfo()
        //{
        //    var monthChartUrl = String.Format("{0}/stock/{1}/company?token={2}", _apiUrl, security, _apiToken);
        //    WriteToFile(security, monthChartUrl, "company_info.json");
        //}

        ////time-series/REPORTED_FINANCIALS/BNGO?token=Tsk_740270bf5c3a4bf192e4d66ac3fecb8b
        //public void GetQuantitativeData()
        //{
        //    string financials_data = String.Format("{0}/time-series/REPORTED_FINANCIALS/{1}?token={2}", _apiUrl, security, _apiToken);
        //    WriteToFile(security, financials_data, "financials_data.json");
        //}

        ////stock/BNGO/advanced-stats?token=Tsk_740270bf5c3a4bf192e4d66ac3fecb8b
        public AdvancedStatsModel GetAdvanceStats()
        {
            var monthChartUrl = String.Format("{0}/stock/{1}/advanced-stats?token={2}", _apiUrl, security, _apiToken);
            var jsonResponse = GetResponse(security, monthChartUrl);

            AdvancedStatsModel advanceStats = JsonConvert.DeserializeObject<AdvancedStatsModel>(jsonResponse);
            return advanceStats;
        }


        public string GetResponse(string security, string url)
        {
            var apiRequest = new HttpRequestMessage(HttpMethod.Get, url);
            using (HttpClient client = new HttpClient())
            {
                return (new WebClient()).DownloadString(url);

            }
        }

    }
}
