﻿using System;
using System.Net;
using System.Net.Http;
using stock_predictions_api.Configeration;

namespace stock_predictions_api.Libraries
{
    public class AlphaVantageApiHandler
    {
        public string security { get; set; }

        private readonly string _apiUrl;
        private readonly string _apiKey;

        public AlphaVantageApiHandler(IConfigurationManager iconfiguration)
        {
            _apiUrl = iconfiguration.AlphaVantageUrl;
            _apiKey = iconfiguration.AlphaVantageKey;
        }

        //https://www.alphavantage.co/query?function=OVERVIEW&symbol=IBM&apikey=demo
        

        private string WriteToFile(string security, string url)
        {
            var apiRequest = new HttpRequestMessage(HttpMethod.Get, url);
            using (HttpClient client = new HttpClient())
            {
                return (new WebClient()).DownloadString(url);
            }
        }
    }
}
