﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using HtmlAgilityPack;
using Newtonsoft.Json;
using stock_predictions_api.Classes;
using stock_predictions_api.Models;
using stock_predictions_api.Models.DB;
using stock_predictions_api.Models.Finviz;

namespace stock_predictions_api.Libraries
{
    public class WebScrapHandler
    {
        private string _security;
        private HtmlDocument _webPage;
        
        private StringHandler _stringHandler;
        private readonly SnapShotData snapShot;

        public WebScrapHandler(string security, StockPredictionDBContext dbContext)
        {
            _security = security;
            _stringHandler = new StringHandler();
            snapShot = new SnapShotData(dbContext);
        }

        public void GetSecuritySnapShot()
        {
            _webPage = new HtmlDocument();
            var apiRequest = new HttpRequestMessage(HttpMethod.Get, String.Format("https://finviz.com/quote.ashx?t={0}", _security));
            using (HttpClient client = new HttpClient())
            {
                var response = client.GetAsync(String.Format("https://finviz.com/quote.ashx?t={0}", _security)).Result;
                var htmlPage = response.Content.ReadAsStringAsync().Result;
                _webPage.LoadHtml(htmlPage);
            }
            
        }

        public FinvizModel SaveSnapShot()
        {
            FinvizModel finvizModel = new FinvizModel();
            var evaluation_data = new Dictionary<string, double>();
            var security_snapshot = new ExpandoObject() as IDictionary<string, object>;
            var table = _webPage.DocumentNode.SelectNodes("//table[@class='snapshot-table2']").ToList();
            var tableRows = table[0].SelectNodes("//tr[@class='table-dark-row']").ToList();
            foreach (var row in tableRows)
            {

                var rowTdsLabel = row.SelectNodes("//td[@class='snapshot-td2-cp']").ToList();
                var rowTdsData = row.SelectNodes("//td[@class='snapshot-td2']").ToList();
                
                for (int i = 0; i < rowTdsData.Count; i += 1)
                {
                    var snapShotProperty = _stringHandler.prettyStringJson(rowTdsLabel[i].InnerText);
                    Console.WriteLine("snapShotProperty => {0}", snapShotProperty);
                    if (!security_snapshot.ContainsKey(snapShotProperty))
                    {
                        security_snapshot.Add(_stringHandler.prettyStringJson(rowTdsLabel[i].InnerText), rowTdsData[i].InnerText);
                        if(rowTdsLabel[i].InnerText == "P/B" && rowTdsData[i].InnerText != "-")
                        {
                            var priceToBookData = snapShot.Get(new SnapShotDataModel { security = _security, label = "Price To Book" });
                            Console.WriteLine("price book data => {0}", priceToBookData);
                            if(priceToBookData == null)
                            {
                                snapShot.Add(new SnapShotDataModel { security = _security, label = "Price To Book", data = rowTdsData[i].InnerText });
                            }
                        }

                        if(rowTdsLabel[i].InnerText == "ROE" && rowTdsData[i].InnerText != "-")
                        {
                            finvizModel.return_on_equity = Convert.ToDouble(rowTdsData[i].InnerText.Replace("%", ""));
                        }

                        if (rowTdsLabel[i].InnerText == "EPS (ttm)" && rowTdsData[i].InnerText != "-")
                        {
                            //evaluation_data.Add("equity_per_share", Convert.ToDouble(rowTdsData[i].InnerText));
                        }

                        if(rowTdsLabel[i].InnerText == "Debt/Eq" && rowTdsData[i].InnerText != "-")
                        {
                            //evaluation_data.Add("debt_to_equity_ratio", Convert.ToDouble(rowTdsData[i].InnerText));
                        }

                        if(rowTdsLabel[i].InnerText == "ROI" && rowTdsData[i].InnerText != "-")
                        {
                            //evaluation_data.Add("return_on_investment", Convert.ToDouble(rowTdsData[i].InnerText.Replace("%", "")));
                        }

                        if (rowTdsLabel[i].InnerText == "Insider Own" && rowTdsData[i].InnerText != "-")
                        {
                            //evaluation_data.Add("insider_ownership", Convert.ToDouble(rowTdsData[i].InnerText.Replace("%", "")));
                        }
                    }

                    
                }
            }
            //_securityContext.EvaluateInvestmentTime(evaluation_data);
            return finvizModel;
        }
    }
}

