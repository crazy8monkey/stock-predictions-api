﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using HtmlAgilityPack;
using stock_predictions_api.Models.DB;

namespace stock_predictions_api.Libraries
{
    public class FinvizHandler
    {
        private Dictionary<string, string> filters;
        private Dictionary<string, string> thresholds;
        private HtmlDocument _webPage;

        public FinvizHandler()
        {
            filters = new Dictionary<string, string>()
            {
                { "Debt to Equity", "fa_debteq" },
                { "Price to Book", "fa_pb" },
                { "Price To Earnings", "fa_pe" },
                { "Return on Equity", "fa_roe" },
                { "Return on Investment", "fa_roi" },
                { "Insider Ownership", "sh_insiderown" }
            };

            thresholds = new Dictionary<string, string>()
            {
                { "<", "u" },
                { ">", "o" }
            };
        }

        public void getSecurityRequest(List<AppSettingStockTypeDataModel> fundamentals)
        {
            StringBuilder urlParams = new StringBuilder();
            foreach (AppSettingStockTypeDataModel fundamental in fundamentals)
            {
                urlParams.AppendFormat("{0}_{1}{2},", filters[fundamental.attribute], thresholds[fundamental.greater_lower], fundamental.data);
            }

            var screenerUrl = String.Format("https://finviz.com/screener.ashx?v=111&f={0}sh_price_u15&ft=4", urlParams.ToString());
            Console.WriteLine("screenerUrl => {0}", screenerUrl);

            //_webPage = new HtmlDocument();
            //using (HttpClient client = new HttpClient())
            //{
            //    var response = client.GetAsync(String.Format("https://finviz.com/quote.ashx?t={0}", _security)).Result;
            //    var htmlPage = response.Content.ReadAsStringAsync().Result;
            //    _webPage.LoadHtml(htmlPage);
            //}
        }


        //public void GetSecuritySnapShot()
        //{
        //    _webPage = new HtmlDocument();
        //    var apiRequest = new HttpRequestMessage(HttpMethod.Get, String.Format("https://finviz.com/quote.ashx?t={0}", _security));
        //    using (HttpClient client = new HttpClient())
        //    {
        //        var response = client.GetAsync(String.Format("https://finviz.com/quote.ashx?t={0}", _security)).Result;
        //        var htmlPage = response.Content.ReadAsStringAsync().Result;
        //        _webPage.LoadHtml(htmlPage);
        //    }

        //}

    }
}
