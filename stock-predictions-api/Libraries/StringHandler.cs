﻿using System;
using System.Text.RegularExpressions;

namespace stock_predictions_api.Libraries
{
    public class StringHandler
    {
        public StringHandler()
        {
        }

        public string prettyStringJson(string label)
        {
            string newLabel = label.ToLower().Replace(" ", "_");
            return Regex.Replace(newLabel, "[^a-zA-Z0-9_.]+", "", RegexOptions.Compiled);
        }
    }
}
