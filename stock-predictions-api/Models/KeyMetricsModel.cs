﻿using System;
using System.Runtime.InteropServices;

namespace stock_predictions_api.Models
{
    public class KeyMetricsModel
    {
        //Price To earnings Ratio
        public decimal? peRatio { get; set; }
        //price to sales
        public decimal? priceToSalesRatio { get; set; }
        //price to book
        public decimal? ptbRatio { get; set; }
        //price cash per share
        public decimal? cashPerShare { get; set; }
        //return on equity
        public decimal? roe { get; set; }
        //debt to equity
        public decimal? debtToEquity { get; set; }
        //earnings per share
        public decimal? revenuePerShare { get; set; }
    }
}


/*
   "symbol" : "AAPL",
  "date" : "2020-12-26",
  "revenuePerShare" : 6.5803493911085,
  "netIncomePerShare" : 1.6979508676614554,
  "operatingCashFlowPerShare" : 2.288912171210607,
  "freeCashFlowPerShare" : 2.0822410518638814,
  "cashPerShare" : 2.126350573621597,
  "bookValuePerShare" : 3.9104537736050156,
  "tangibleBookValuePerShare" : 20.90649613976731,
  "shareholdersEquityPerShare" : 3.9104537736050156,
  "interestDebtPerShare" : 6.653688114030968,
  "marketCap" : 2314861449980.238,
  "enterpriseValue" : 2390894449980.238,
  "peRatio" : 80.50291949157496,
  "priceToSalesRatio" : 20.772453539427293,
  "pocfratio" : 59.718325464495464,
  "pfcfRatio" : 65.64561863653795,
  "pbRatio" : 34.95502310310821,
  "evToSales" : 21.45473712057931,
  "enterpriseValueOverEBITDA" : 64.8237521345942,
  "evToOperatingCashFlow" : 61.679809353771326,
  "evToFreeCashFlow" : 67.80178799252015,
  "earningsYield" : 0.012421909743343595,
  "freeCashFlowYield" : 0.015233309103791523,
  "debtToEquity" : 4.346309495047112,
  "debtToAssets" : 0.8129550859473413,
  "netDebtToEBITDA" : 2.061464631402001,
  "currentRatio" : 1.16300270929083,
  "interestCoverage" : 52.63166144200627,
  "incomeQuality" : 1.3480438184663537,
  "dividendYield" : 0.0015607845558233493,
  "payoutRatio" : 0.12564771344114067,
  "salesGeneralAndAdministrativeToRevenue" : 0.0,
  "researchAndDdevelopementToRevenue" : 0.04633027934565098,
  "intangiblesToTotalAssets" : 0.0,
  "capexToOperatingCashFlow" : -11.075142857142858,
  "capexToRevenue" : -31.839714285714287,
  "capexToDepreciation" : -0.7617142857142857,
  "stockBasedCompensationToRevenue" : 0.018126508672906255,
  "grahamNumber" : 12.222706881106967,
  "roic" : 0.151566033392463,
  "returnOnTangibleAssets" : 0.0812164246131946,
  "grahamNetNet" : -9.716642676086304,
  "workingCapital" : 21599000000,
  "tangibleAssetValue" : null,
  "netCurrentAssetValue" : -133724000000,
  "investedCapital" : null,
  "averageReceivables" : 48032500000,
  "averagePayables" : 53071000000,
  "averageInventory" : 4517000000,
  "daysSalesOutstanding" : 47.34249230520733,
  "daysPayablesOutstanding" : 85.62143314806812,
  "daysOfInventoryOnHand" : 6.669100445530539,
  "receivablesTurnover" : 1.9010406004776528,
  "payablesTurnover" : 1.0511386774425961,
  "inventoryTurnover" : 13.495073396340237,
  "roe" : 0.43420814206330033,
  "capexPerShare" : -0.20667111934672558
 
 */