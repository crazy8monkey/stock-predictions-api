﻿using System;
namespace stock_predictions_api.Models.FinancialPrep
{
    public class CompanyInfoModel
    {
        public string sector { get; set; }
        public string price { get; set; }
        public long mktCap { get; set; }
        //trade data = average volume
        public Int64 volAvg { get; set; }

    }
}

/*
{
  "volume":10450146,
}

 */

