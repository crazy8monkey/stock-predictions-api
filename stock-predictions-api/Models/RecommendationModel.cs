﻿using System;
using System.ComponentModel.DataAnnotations;

namespace stock_predictions_api.Models
{
    public class RecommendationModel
    {
        [Key]
        public int id { get; set; }
        public string label { get; set; }
        public decimal min { get; set; }
        public decimal max { get; set; }
        public string sector { get; set; }
    }
}
