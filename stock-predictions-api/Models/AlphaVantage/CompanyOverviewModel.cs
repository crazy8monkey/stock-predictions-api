﻿using System;
namespace stock_predictions_api.Models.AlphaVantage
{
    public class CompanyOverviewModel
    {
        public string PERatio { get; set; }
        public string PriceToSalesRatioTTM { get; set; }
        public string PriceToBookRatio { get; set; }
        public string ReturnOnEquityTTM { get; set; }
        public string EPS { get; set; }
    }
}
