﻿using System;
using System.Collections.Generic;

namespace stock_predictions_api.Models.Finviz
{
    public class Screener
    {
        public List<String> securities { get; set; }
    }
}
