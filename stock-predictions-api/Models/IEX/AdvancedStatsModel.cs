﻿using System;
namespace stock_predictions_api.Models.IEX
{
    public class AdvancedStatsModel
    {
        public decimal? priceToSales { get; set; }
        public decimal? priceToBook { get; set; }
        public decimal? debtToEquity { get; set; }
        public decimal? pegRatio { get; set; }

        //"beta": 1.4661365583766115,
        //"totalCash": 66301000000,
        //"currentDebt": 20748000000,
        //"revenue": 265809000000,
        //"grossProfit": 101983000000,
        //"totalRevenue": 265809000000,
        //"EBITDA": 80342000000,
        //"revenuePerShare": 0.02,
        //"revenuePerEmployee": 2013704.55,

        //"profitMargin": 22.396157,
        //"enterpriseValue": 1022460690000,
        //"enterpriseValueToRevenue": 3.85,

        //"forwardPERatio": 18.14,

        //"peHigh": 22.61,
        //"peLow": 11.98,
        //"week52highDate": "2019-11-19",
        //"week52lowDate": "2019-01-03",
        //"week52highDateSplitAdjustOnly": null,
        //"week52lowDateSplitAdjustOnly": null,
        //"putCallRatio": 0.7611902044412406,
    }
}
