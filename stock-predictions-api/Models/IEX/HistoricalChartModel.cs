﻿using System;
namespace stock_predictions_api.Models.IEX
{
    public class HistoricalChartModel
    {
        public decimal close { get; set; }
        public float high { get; set; }
        public float low { get; set; }
        public float open { get; set; }
        public string symbol { get; set; }
        public decimal volume { get; set; }
        public string id { get; set; }
        public string key { get; set; }
        public string subkey { get; set; }
        public string date { get; set; }
        public double updated { get; set; }
        public float changeOverTime { get; set; }
        public float marketChangeOverTime { get; set; }
        public float uOpen { get; set; }
        public float uClose { get; set; }
        public float uHigh { get; set; }
        public float uLow { get; set; }
        public int uVolume { get; set; }
        public float fOpen { get; set; }
        public float fClose { get; set; }
        public float fHigh { get; set; }
        public float fLow { get; set; }
        public decimal fVolume { get; set; }
        public string label { get; set; }
        public float change { get; set; }
        public float changePercent { get; set; }
    }
}
