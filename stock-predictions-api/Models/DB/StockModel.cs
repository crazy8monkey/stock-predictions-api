﻿using System;
using System.ComponentModel.DataAnnotations;

namespace stock_predictions_api.Models.DB
{
    public class StockModel
    {
        [Key]
        public int id { get; set; }
        public string short_code { get; set; }
        public Nullable<DateTime> last_modified { get; set; }
        public string sector { get; set; }
        public Nullable<decimal> three_month_close { get; set; }
        public Nullable<decimal> six_month_close { get; set; }
        public Nullable<decimal> nine_month_close { get; set; }
        public Nullable<decimal> current_price { get; set; }
        public Nullable<long>  market_cap { get; set; }
    }
}
