﻿using System;
using System.ComponentModel.DataAnnotations;

namespace stock_predictions_api.Models.DB
{
    public class AppSettingStockTypeModel
    {
        [Key]
        public int id { get; set; }
        public string name { get; set; }
    }
}
