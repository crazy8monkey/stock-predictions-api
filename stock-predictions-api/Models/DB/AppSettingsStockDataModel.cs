﻿using System;
using System.ComponentModel.DataAnnotations;

namespace stock_predictions_api.Models.DB
{
    public class AppSettingsStockDataModel
    {
        [Key]
        public int id { get; set; }
        public string label { get; set; }
        public Boolean is_active { get; set; }
    }
}
