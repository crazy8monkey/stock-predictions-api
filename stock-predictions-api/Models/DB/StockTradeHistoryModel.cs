﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace stock_predictions_api.Models.DB
{
    public class StockTradeHistoryModel
    {
        [Key]
        public int trade_id { get; set; }
        public string security { get; set; }
        public string trade_json { get; set; }
        public string trade_sector { get; set; }
        public decimal trade_purchase_price { get; set; }
        public Nullable<decimal> trade_selling_price { get; set; }
        public DateTime trade_purchase_date { get; set; }
        public Nullable<DateTime> trade_selling_date { get; set; }
        public Nullable<decimal> trade_return { get; set; }
        public int trade_days_length { get; set; }
        public string market_cap { get; set; }
    }

    public class JsonTradeData {
        public object data { get; set; }
    }
}
