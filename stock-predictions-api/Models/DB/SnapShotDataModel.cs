﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace stock_predictions_api.Models.DB
{
    public class SnapShotDataModel
    {
        [Key]
        public int id { get; set; }
        public string security { get; set; }
        public string label { get; set; }
        public string data { get; set; }
        public Nullable<DateTime> updated_date { get; set; }
        public string update_type { get; set; }
    }

    public class UpdateSnapShotData
    {
        public List<SnapShotDataModel> new_data { get; set; }
    }
}
