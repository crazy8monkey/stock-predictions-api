﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace stock_predictions_api.Models.DB
{
    public class AppSettingStockTypeDataModel
    {
        [Key]
        public int id { get; set; }
        public int stock_type_id { get; set; }
        public string attribute { get; set; }
        public string greater_lower { get; set; }
        public string data { get; set; }
    }
}
