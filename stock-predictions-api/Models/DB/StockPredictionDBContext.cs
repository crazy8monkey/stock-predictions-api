﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using stock_predictions_api.Configeration;
using stock_predictions_api.Models.DB;

namespace stock_predictions_api.Models.DB
{
    public class StockPredictionDBContext : DbContext
    {

        private readonly IConfigurationManager _configuration;

        public StockPredictionDBContext(IConfigurationManager iconfiguration)
        {
            _configuration = iconfiguration;
        }

        public IConfigurationManager GetAppSettings()
        {
            return _configuration;
        }

        public virtual DbSet<StockModel> Security { get; set; }
        public virtual DbSet<AppSettings> Settings { get; set; }
        public virtual DbSet<AppSettingsStockDataModel> DataSettings { get; set; }
        public virtual DbSet<StockTradeHistoryModel> TradingHistory { get; set; }
        public virtual DbSet<SnapShotDataModel> SnapShotData { get; set; }
        public virtual DbSet<RecommendationModel> Recommendation { get; set; }
        public virtual DbSet<AppSettingStockTypeModel> SettingStockTypes { get; set; }
        public virtual DbSet<AppSettingStockTypeDataModel> SettingStockTypesData { get; set; }
        

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_configuration.DBConnection);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("dbo");
            modelBuilder.Entity<StockModel>().ToTable("stock_list");
            modelBuilder.Entity<AppSettings>().ToTable("app_settings");
            modelBuilder.Entity<StockTradeHistoryModel>().ToTable("stock_trade_history");
            modelBuilder.Entity<SnapShotDataModel>().ToTable("snap_shot_data");
            modelBuilder.Entity<RecommendationModel>().ToTable("recommendation");
            modelBuilder.Entity<AppSettingsStockDataModel>().ToTable("app_settings_stock_data");
            modelBuilder.Entity<AppSettingStockTypeModel>().ToTable("app_settings_stock_types");
            modelBuilder.Entity<AppSettingStockTypeDataModel>().ToTable("app_settings_stock_types_attributes");
            base.OnModelCreating(modelBuilder);
        }
    }
}
