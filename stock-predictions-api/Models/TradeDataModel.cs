﻿using System;
using Microsoft.ML.Data;

namespace stock_predictions_api.Models
{
    /*
     * valuable information
     * https://docs.microsoft.com/en-us/dotnet/api/microsoft.ml.textcatalog.featurizetext?view=ml-dotnet
     * https://docs.microsoft.com/en-us/dotnet/api/microsoft.ml.normalizationcatalog.normalizeminmax?view=ml-dotnet
     * https://github.com/dotnet/machinelearning/blob/master/docs/code/MlNetCookBook.md
     * https://docs.microsoft.com/en-us/dotnet/api/microsoft.ml.categoricalcatalog.onehotencoding?view=ml-dotnet
     */

    public class TradeDataLearn
    {
        [LoadColumn(0)] public float ReturnPercentage;
        [LoadColumn(1)] public string SnapShotLabel;
        [LoadColumn(2)] public float SnapShotValue;
    }

    public class TradeDataPrediction
    {
        [ColumnName("Score")]
        public float SnapShotValue;
    }
}
