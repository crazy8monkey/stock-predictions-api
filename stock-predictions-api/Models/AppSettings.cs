﻿using System;
using System.ComponentModel.DataAnnotations;

namespace stock_predictions_api.Models
{
    public class AppSettings
    {
        [Key]
        public int settings_id { get; set; }
        public int target_growth { get; set; }
    }
}
